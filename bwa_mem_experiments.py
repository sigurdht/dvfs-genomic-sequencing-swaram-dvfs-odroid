import argparse, inspect, multiprocessing, os, re, subprocess, time

from mount_usb import mount_genstore
from cpu_frequency import set_cpu_frequency
from cpu_on_off import power_off_cpu, power_on_cpu
from power_data import PowerMonitorLogParser, BwaDvfsCsvEntry, read_bwa_dvfs_csv_entries_from_file


def run_bwa_mem(fa_file_path, fq_file_path, output_file_path, t=8, K=80_000):
    """
    Description of parameters:

    -t INT        number of threads [1]
    -K INT        process INT input bases in each batch regardless of nThreads (for reproducibility) []
    """
    # command = f"/home/odroid/swaram/bwa-arm/bwa mem -t {t} -K {K} {fa_file_path} {fq_file_path} > {output_file_path}"
    command = f"/home/odroid/swaram/bwa-arm/bwa mem -t {t} -K {K} {fa_file_path} {fq_file_path} > {output_file_path}"
    print(">", command)
    subprocess.run(command, shell=True)


def start_power_measurement(serial_device_path, log_file_path):
    stop_measurement()
    subprocess.run(f"rm {log_file_path}", shell=True)
    command = f"screen -dmS power_monitor minicom -D {serial_device_path} -C {log_file_path}"
    print(">", command)
    subprocess.run(command, shell=True)


def is_measurement_running():
    # I have no fucking clue why `check_output` doesn't work:
    # output = subprocess.check_output("screen -ls", shell=True).decode("UTF-8")
    output = subprocess.run("screen -ls", shell=True, stdout=subprocess.PIPE).stdout.decode("UTF-8")
    for line in output.split("\n"):
        match = re.match(r".*?(\d+)\.power_monitor.*", line)
        if match is None:
            continue
        (process_id,) = match.groups()
        return process_id
    return False


def stop_measurement():
    process_id = is_measurement_running()
    while process_id is not False:
        print("Killing process", process_id)
        subprocess.run(f"kill {process_id}", shell=True)
        time.sleep(1)
        process_id = is_measurement_running()


def run_bwa_mem_experiment(
    cpu_0_to_3_frequency_khz=1.5e6,
    cpu_4_to_7_frequency_khz=2.0e6,
    smart_power_serial_usb_device_path="/dev/ttyUSB0",
    power_monitor_log_file_path="/home/odroid/power_monitor.log",
    short_read_file_path="/genstore/single.fq",
    reference_genome_file_path="/genstore/Reference_part1/hs37d5_split.fa",
    bwa_mem_output_file_path="/home/odroid/bwa_mem_output.txt",
    result_csv_file_path="/home/odroid/test_results.csv",
    room_temperature=20,
    number_of_threads=8,
    number_of_input_bases_per_batch=80_000,
    power_cpu_0_off=False,
    power_cpu_1_off=False,
    power_cpu_2_off=False,
    power_cpu_3_off=False,
    power_cpu_4_off=False,
    power_cpu_5_off=False,
    power_cpu_6_off=False,
    power_cpu_7_off=False,
    skip_if_already_in_results_csv_file=True,
):
    local_variables = locals()
    cpu_power_statuses = [int(eval(f"power_cpu_{n}_off", {}, local_variables) is False) for n in range(8)]
    csv_entry_parameters = [
        short_read_file_path,
        reference_genome_file_path,
        cpu_0_to_3_frequency_khz,
        cpu_4_to_7_frequency_khz,
        room_temperature,
        number_of_threads,
        number_of_input_bases_per_batch,
        *cpu_power_statuses,
    ]
    if skip_if_already_in_results_csv_file:
        existing_entries = read_bwa_dvfs_csv_entries_from_file(result_csv_file_path)
        for entry in existing_entries:
            if csv_entry_parameters == entry.get_parameters():
                print(f"Configuration is already in CSV file, skipping ({csv_entry_parameters})")
                return

    for n, power_status in enumerate(cpu_power_statuses):
        if power_status == 0:
            print(f"POWERING OFF CPU {n}")
            power_off_cpu(n)
            print(f"POWERED OFF CPU {n}")

    set_cpu_frequency(0, cpu_0_to_3_frequency_khz)
    set_cpu_frequency(4, cpu_4_to_7_frequency_khz)
    start_power_measurement(smart_power_serial_usb_device_path, power_monitor_log_file_path)
    run_bwa_mem(
        reference_genome_file_path,
        short_read_file_path,
        bwa_mem_output_file_path,
        t=number_of_threads,
        K=number_of_input_bases_per_batch,
    )
    stop_measurement()
    power_consumption = PowerMonitorLogParser(power_monitor_log_file_path)
    csv_entry = BwaDvfsCsvEntry(
        *csv_entry_parameters,
        power_consumption.duration_seconds(),
        power_consumption.total_watthours(),
    )
    csv_entry.store_to_file(result_csv_file_path)

    for n, power_status in enumerate(cpu_power_statuses):
        if power_status == 0:
            print(f"POWERING ON CPU {n}")
            power_on_cpu(n)
            print(f"POWERED ON CPU {n}")

    # Sleep 15 seconds to make sure all connections with the serial USB device are properly stopped.
    time.sleep(15)

    return csv_entry


CONFIGURATION_SET_1 = [
    *({
        "cpu_4_to_7_frequency_khz": cpu_4_to_7_frequency_kilohertz,
    } for cpu_4_to_7_frequency_kilohertz in [
        2.0e6, 1.9e6, 1.8e6, 1.7e6, 1.6e6, 1.5e6, 1.4e6, 1.3e6, 1.2e6, 1.1e6, 1.0e6, 0.9e6, 0.8e6, 0.7e6, 0.6e6, 0.5e6,
    ]),
    *({
        "cpu_0_to_3_frequency_khz": cpu_0_to_3_frequency_kilohertz,
    } for cpu_0_to_3_frequency_kilohertz in [
        1.5e6, 1.4e6, 1.3e6, 1.2e6, 1.1e6, 1.0e6, 0.9e6, 0.8e6, 0.7e6, 0.6e6, 0.5e6,
    ]),
]


CONFIGURATION_SET_2 = [
    *({
        "skip_if_already_in_results_csv_file": False,
        "result_csv_file_path": "/home/odroid/test_results_2.csv",
        "cpu_4_to_7_frequency_khz": cpu_4_to_7_frequency_kilohertz,
    } for cpu_4_to_7_frequency_kilohertz in [
        2.0e6, 1.9e6, 1.8e6, 1.7e6, 1.6e6, 1.5e6, 1.4e6, 1.3e6, 1.2e6, 1.1e6, 1.0e6,
    ]),
]


def main(configuration_set_number: int, start_position: int=0, loop_number_of_times: int=1):
    """
    `start_position` is the index of the configuration in the configuration set to start the iteration at. This might be useful when you need to resume execution of a configuration set from a previous experiment running session.
    """
    print("Mounting USB")
    mount_genstore()
    print("Running BWA-MEM experiments")
    configuration_sets = [None, CONFIGURATION_SET_1, CONFIGURATION_SET_2]
    for i in range(loop_number_of_times):
        configuration_set = configuration_sets[configuration_set_number]
        if i == 0:
            configuration_set = configuration_set[start_position:]
        for configuration in configuration_set:
            print("Testing configuration:", configuration)
            run_bwa_mem_experiment(**configuration)

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("configuration_set_number", type=int)
    argument_parser.add_argument("--start-position", "-s", type=int, default=0)
    argument_parser.add_argument("--loop-number-of-times", "-n", type=int, default=1)
    arguments = argument_parser.parse_args()
    main(
        arguments.configuration_set_number,
        start_position=arguments.start_position,
        loop_number_of_times=arguments.loop_number_of_times,
    )
