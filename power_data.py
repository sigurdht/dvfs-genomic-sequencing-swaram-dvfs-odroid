import argparse, inspect, multiprocessing, os, re, subprocess, time

from cpu_frequency import set_cpu_frequency
from cpu_on_off import power_off_cpu, power_on_cpu


class PowerRead:
    voltage: float
    current: float
    wattage: float

    def __init__(
        self,
        voltage,
        current,
        wattage,
    ):
        self.voltage = voltage
        self.current = current
        self.wattage = wattage


class PowerMonitorLogParser:
    reads: list

    def __init__(self, power_monitor_log_file_path: str):
        self.reads = []

        with open(power_monitor_log_file_path, "r") as file:
            for line in file.readlines():
                voltage, current, wattage, _ = line.split(",")
                self.reads.append(
                    PowerRead(
                        voltage=float(voltage),
                        current=float(current),
                        wattage=float(wattage),
                    )
                )

    def duration_seconds(self):
        return len(self.reads)

    def total_watthours(self):
        return sum(read.wattage for read in self.reads) / (60 * 60)
    
    def average_wattage(self):
        if len(self.reads) == 0:
            return 0
        return self.total_watthours() / len(self.reads) * 60 * 60
    

class BwaDvfsCsvEntry:
    csv_parameters_format = [
        ("short_read_file", str),
        ("reference_genome_file", str),
        ("cpu_0_to_3_frequency_kilohertz", int),
        ("cpu_4_to_7_frequency_kilohertz", int),
        ("room_temperature_celsius", float),
        ("number_of_threads", int),
        ("number_of_input_bases_per_batch", int),
        ("cpu_0_power_status", int),
        ("cpu_1_power_status", int),
        ("cpu_2_power_status", int),
        ("cpu_3_power_status", int),
        ("cpu_4_power_status", int),
        ("cpu_5_power_status", int),
        ("cpu_6_power_status", int),
        ("cpu_7_power_status", int),
    ]

    csv_results_format = [
        ("time_usage_seconds", float),
        ("total_energy_consumption_watthours", float),
    ]

    def __init__(
        self,
        *values,
    ):
        for i, (data_name, data_type) in enumerate(self.csv_format()):
            setattr(self, data_name, data_type(values[i]))

    @classmethod
    def csv_format(cls):
        return cls.csv_parameters_format + cls.csv_results_format

    def average_power_watts(self):
        return self.total_energy_consumption_watthours * 60 * 60 / self.time_usage_seconds

    def to_list(self):
        return [
            getattr(self, data_name) for (data_name, _) in self.csv_format()
        ]
    
    def get_csv_header(self):
        return [",".join([data_name for (data_name, _) in self.csv_format()])]

    def to_csv_line(self):
        return ",".join([str(item) for item in self.to_list()])
    
    def get_parameters(self):
        return [
            getattr(self, data_name) for (data_name, _) in self.csv_parameters_format
        ]
    
    def store_to_file(self, csv_file_path: str):
        try:
            with open(csv_file_path, "r") as file:
                csv_data = file.read().split("\n")
        except FileNotFoundError:
            csv_data = self.get_csv_header()
        
        while csv_data[-1] == "":
            csv_data = csv_data[:-1]

        csv_data.append(self.to_csv_line())

        with open(csv_file_path, "w") as file:
            file.write("\n".join(csv_data) + "\n")


def read_bwa_dvfs_csv_entries_from_file(csv_file_path: str):
    try:
        with open(csv_file_path, "r") as file:
            csv_data = file.read().split("\n")[1:]
    except FileNotFoundError:
        csv_data = []
    
    while len(csv_data) > 0 and csv_data[-1] == "":
        csv_data = csv_data[:-1]

    return [
        BwaDvfsCsvEntry(*csv_line.split(",")) for csv_line in csv_data
    ]
