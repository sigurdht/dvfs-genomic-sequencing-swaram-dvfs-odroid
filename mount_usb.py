import argparse, inspect, multiprocessing, os, re, subprocess, time


def mount_usb(usb_device_path, mount_directory):
    subprocess.call(f"sudo mount {usb_device_path} {mount_directory}", shell=True)


def mount_genstore():
    mount_usb("/dev/sda1", "/genstore/")


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    argument_parser = argparse.ArgumentParser()
    for parameter in inspect.signature(mount_usb).parameters:
        argument_parser.add_argument(parameter)
    arguments = argument_parser.parse_args()
    mount_usb(**vars(arguments))
