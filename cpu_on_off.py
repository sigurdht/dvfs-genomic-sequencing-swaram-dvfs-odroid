import argparse, inspect, multiprocessing, os, re, subprocess, time


def set_cpu_online_status(cpu_num, status):
    """
    Sets online status of `cpu_num` to `status`.
    `cpu_num` must be either 1, 2, 3, 4, 5, 6 or 7.
    `status` must be either 0 or 1.
    """
    if cpu_num not in [1, 2, 3, 4, 5, 6, 7]:
        raise Exception(f"cpu_num={cpu_num} is not valid. Must be either 1, 2, 3, 4, 5, 6 or 7")
    if status not in [0, 1]:
        raise Exception(f"status={status} is not valid. Must be either 0 or 1")
    subprocess.call(f'sudo -S sh -c "echo {status} > /sys/devices/system/cpu/cpu{cpu_num}/online"', shell=True)


def power_off_cpu(cpu_num):
    print("Powering off CPU", cpu_num)
    set_cpu_online_status(cpu_num, 0)


def power_on_cpu(cpu_num):
    print("Powering on CPU", cpu_num)
    set_cpu_online_status(cpu_num, 1)


def power_off_all_cpus_except_0():
    for cpu_num in range(1, 8):
        power_on_cpu(cpu_num)


def power_on_all_cpus():
    for cpu_num in range(1, 8):
        power_off_cpu(cpu_num)


def power_off_cpu_4_to_7():
    for cpu_num in range(4, 8):
        power_off_cpu(cpu_num)


def power_on_cpu_4_to_7():
    for cpu_num in range(4, 8):
        power_on_cpu(cpu_num)
